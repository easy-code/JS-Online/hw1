## Email after first lesson

##### Cсылка на видео
[Video](https://drive.google.com/open?id=1aAUoqW6faBhBUef6ILtLXhGNSeDJyJPY)

##### Ссылки на презентацию
[First](https://docs.google.com/presentation/d/1S3_dCe3Wy08uTPFv2G8HzpV0KWRHe0B4hYN6l1Hg4nc/pub?start=false&loop=false&delayms=3000&slide=id.gf7ffa5058_1_267)

[Second](https://docs.google.com/presentation/d/1itjhzpMjxLYgdj0Mv-mGaFq7gu4D9KzFVrLrVXUo64o/pub?start=false&loop=false&delayms=3000&slide=id.p)

##### Справочники
[developer.mozilla.org](https://developer.mozilla.org/uk/docs/Web/JavaScript)

[learn.javascript.ru](https://learn.javascript.ru/getting-started)

##### Стиль кода
https://learn.javascript.ru/coding-style

##### Ссылки из презентации
[ECMAScript](http://www.ecma-international.org/ecma-262/6.0/index.html#sec-ecmascript-language-types)

[null and undefined](https://frontender.info/exploring-the-abyss-of-null-and-undefined-in-javascript/)

[lastIndexOf](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/String/lastIndexOf)

[number](https://learn.javascript.ru/number)

[NaN](https://ru.wikipedia.org/wiki/NaN)

[typeOf](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Operators/typeof)
