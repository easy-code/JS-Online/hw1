//  Числа

// --------------------------------------------------------------------------------------------------------------
// 1. Получить число pi из Math и округлить его до 2-х знаков после точки
// --------------------------------------------------------------------------------------------------------------
const PI = Math.PI;
console.log(Math.round(PI * 100) / 100); // 3.14

// --------------------------------------------------------------------------------------------------------------
// 2. Используя Math, найти максимальное и минимальное числа из представленного ряда 15, 11, 16, 12, 51, 12, 13, 51
// --------------------------------------------------------------------------------------------------------------
let numArray = [15, 11, 16, 12, 51, 12, 13, 51];
console.log(Math.max(...numArray)); // 51
console.log(Math.min(...numArray)); // 11

// --------------------------------------------------------------------------------------------------------------
// 3. Работа с Math.random:
//    a. получить случайное число и округлить его до двух цифр после запятой
//    b. получить случайно целое число от 0 до X
// --------------------------------------------------------------------------------------------------------------
let randNum = Math.random();
let roundedA = parseFloat(randNum.toFixed(2));
let roundedB = Math.round(randNum * 100);
console.log(roundedA); // (a)
console.log(roundedB); // (b)

// --------------------------------------------------------------------------------------------------------------
// 4. Проверить результат вычисления 0.6 + 0.7 - как привести к нормальному виду (1.3)?
// --------------------------------------------------------------------------------------------------------------
let a = 0.6,
    b = 0.7;
let result = a + b;
console.log(result); // 1.2999999999999998
console.log(+result.toFixed(10)); // 1.3
console.log((a * 10 + b * 10) / 10); // 1.3

// --------------------------------------------------------------------------------------------------------------
// 5. Получить число из строки '100$'
// --------------------------------------------------------------------------------------------------------------
let string = '100$';
console.log(parseInt(string)); // 100
