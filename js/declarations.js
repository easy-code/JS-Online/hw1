// Переменные

// --------------------------------------------------------------------------------------------------------------
// 1. Названия для переменных
// --------------------------------------------------------------------------------------------------------------
const MAX_NUMBER = 10; // максимальное число (постоянное)
const USER_NAME = 'Alexander'; // имя пользователя (постоянное)
let priceNew; // цена (может меняться)
let userInfo; // информация о юзере (может меняться)*/

// --------------------------------------------------------------------------------------------------------------
// 2. Что будет в консоли (два подзадания)
// --------------------------------------------------------------------------------------------------------------
console.log(test); // undefined
var test = 'string';


var x = 'string';
var x = 'string № 2';
console.log(x); // string № 2

// --------------------------------------------------------------------------------------------------------------
// 3. Что будет в консоли (три подзадания)
// --------------------------------------------------------------------------------------------------------------
console.log(test); // ReferenceError: test is not defined. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Not_defined
let test = 'string';


const x = 'string';
x = 'string № 2';
console.log(x); // TypeError: Assignment to constant variable. https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Errors/Invalid_const_assignment


let someVariable = 15;
let someVariable = 10; // SyntaxError: Identifier 'someVariable' has already been declared
